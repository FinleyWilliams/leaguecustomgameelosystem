import com.jayway.jsonpath.JsonPath;

import static com.jayway.jsonpath.JsonPath.read;

public class ReadMatchData {

    public int numberOfParticipants (String JSON) { return JsonPath.read(JSON, "$.participants.length()"); }

}
