import java.io.IOException;

public class main {
    public static void main(String[] args) {
        Core getMatchDetails = new Core();
        try {
            getMatchDetails.getMatchJson();
        } catch (IOException e) {
            e.printStackTrace();
        }
        EloCalc eloCalc = new EloCalc();
        eloCalc.calculateElo("doug");
    }
}
