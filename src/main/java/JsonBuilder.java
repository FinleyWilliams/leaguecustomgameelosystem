import com.google.common.io.Resources;
import com.jayway.jsonpath.JsonPath;
import org.apache.commons.codec.Charsets;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;

class JsonBuilder {

    void BuildJSON(String name, String champInput, int killsInput, int deathsInput, int assistsInput, int goldInput, int matchtimeInput, boolean winInput) {

        String PlayerData = StringFromFile(name);

        JSONObject dataToSend = new JSONObject();

        JSONArray jsonArrayProfile = new JSONArray();

        int Length = JsonPath.read(PlayerData, "$.Matches.length()");
        for (int i = 0; i < Length; i++) {

            String champ = JsonPath.read(PlayerData, "$.Matches[" + i + "].Champion");
            int kills = JsonPath.read(PlayerData, "$.Matches[" + i + "].Kills");
            int deaths = JsonPath.read(PlayerData, "$.Matches[" + i + "].Deaths");
            int assists = JsonPath.read(PlayerData, "$.Matches[" + i + "].Assists");
            int goldEarned = JsonPath.read(PlayerData, "$.Matches[" + i + "].Gold");
            Boolean win = JsonPath.read(PlayerData, "$.Matches[" + i + "].Win");
            int matchTime = JsonPath.read(PlayerData, "$.Matches[ " + i + "].matchTime");

            JSONObject jsonObjectPost1 = new JSONObject();
            jsonObjectPost1.put("Champion", champ);
            jsonObjectPost1.put("matchTime", matchTime);
            jsonObjectPost1.put("Win", win);
            jsonObjectPost1.put("Kills", kills);
            jsonObjectPost1.put("Deaths", deaths);
            jsonObjectPost1.put("Assists", assists);
            jsonObjectPost1.put("Gold", goldEarned);
            jsonArrayProfile.put(jsonObjectPost1);
        }

        //Create the latest game object
        JSONObject jsonObjectPost2 = new JSONObject();
        jsonObjectPost2.put("Champion", champInput);
        jsonObjectPost2.put("matchTime", matchtimeInput);
        jsonObjectPost2.put("Win", winInput);
        jsonObjectPost2.put("Kills", killsInput);
        jsonObjectPost2.put("Deaths", deathsInput);
        jsonObjectPost2.put("Assists", assistsInput);
        jsonObjectPost2.put("Gold", goldInput);
        jsonArrayProfile.put(jsonObjectPost2);

        dataToSend.put("Player", name);
        dataToSend.put("Matches", jsonArrayProfile);

        try {
            PrintWriter out = new PrintWriter("C:/Users/Finley/IdeaProjects/LeagueMatchMaker/src/main/resources/Players/"+name+"Profile.json");
            out.print(dataToSend.toString());
            out.close();
            System.out.println("The game was added to file for the player:" + name);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return;
    }

    private String StringFromFile(String name){

        File tmpDir = new File("C:/Users/Finley/IdeaProjects/LeagueMatchMaker/src/main/resources/Players/"+name+"Profile.json");
        boolean exists = tmpDir.exists();

        if (exists)
        {
            URL url = Resources.getResource("Players/" + name + "Profile.json");
            try {
                return Resources.toString(url, Charsets.UTF_8);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {

            JSONObject dataToSend = new JSONObject();
            JSONArray jsonArrayProfile = new JSONArray();
            dataToSend.put("Player", name);
            dataToSend.put("Matches", jsonArrayProfile);

            try {
                PrintWriter out = new PrintWriter("C:/Users/Finley/IdeaProjects/LeagueMatchMaker/src/main/resources/Players/"+name+"Profile.json");
                out.print(dataToSend.toString());
                out.close();
                System.out.println("No profile found for " + name + ". Creating blank profile...");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return dataToSend.toString();
        }
    return "";
    }
}
