import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class DataRepository {

    private String Endpoint = "https://euw1.api.riotgames.com/lol/match/v3/matches/";
    private String apiKey = "?api_key=RGAPI-f4c46a72-b34b-43da-be8e-c110df91ed8a";

    public String getMatchJSON( String matchID ) {
        String ConstructedUrl;
        try {
            ConstructedUrl = Endpoint + matchID + apiKey;

            HttpResponse<JsonNode> jsonResponse = Unirest.get(ConstructedUrl).asJson();
            String outputJson = String.valueOf(jsonResponse.getBody());

            return outputJson;

        }
        catch (UnirestException e) {
            e.printStackTrace();
        }

        return null;
    }
}
