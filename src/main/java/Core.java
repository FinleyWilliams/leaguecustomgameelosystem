import com.google.common.io.Resources;
import com.jayway.jsonpath.JsonPath;
import org.apache.commons.codec.Charsets;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

class Core {

    private String Endpoint = "https://euw1.api.riotgames.com/lol/match/v3/matches/~MATCHID~?api_key=RGAPI-f4c46a72-b34b-43da-be8e-c110df91ed8a";

    void getMatchJson() throws IOException {

        String name = null;

        do {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String matchID = "3786589366";
            System.out.println("MatchID = ?");

            matchID = reader.readLine();

            if (matchID.equals(""))
            {
                matchID = "3786589366";
            }


            //getMatchID;
            String JSON = null;

            DataRepository dataRepository = new DataRepository();
            ReadMatchData readMatchData = new ReadMatchData();
            JsonBuilder jsonBuilder = new JsonBuilder();

            JSON = dataRepository.getMatchJSON(matchID);

            int numberOfParticipants = readMatchData.numberOfParticipants(JSON);

            for (int i = 0; i < numberOfParticipants; i++) {

                List<String> values = GetValues(JSON, i);

                int champid = Integer.valueOf(values.get(0));
                int kills = Integer.valueOf(values.get(1));
                int deaths = Integer.valueOf(values.get(2));
                int assists = Integer.valueOf(values.get(3));
                int goldEarned = Integer.valueOf(values.get(4));
                boolean win = Boolean.valueOf(values.get(5));
                int matchTime = Integer.valueOf(values.get(6));

                name = GetWhoPlayedChampion(reader, champid);

                if (name.equals("quit"))
                {
                    return;
                }

                String champ = new ChampionIdToChampString().getChampName(champid);

                jsonBuilder.BuildJSON(name, champ, kills, deaths, assists, goldEarned, matchTime, win);
            }
        } while (!name.equals("quit"));
    }

    private List<String> GetValues(String JSON, int i) {

        List<String> Output = new ArrayList<>();

        int champid = JsonPath.read(JSON, "$.participants[" + i + "].championId");
        int kills = JsonPath.read(JSON, "$.participants[" + i + "].stats.kills");
        int deaths = JsonPath.read(JSON, "$.participants[" + i + "].stats.deaths");
        int assists = JsonPath.read(JSON, "$.participants[" + i + "].stats.assists");
        int goldEarned = JsonPath.read(JSON, "$.participants[" + i + "].stats.goldEarned");
        boolean win = JsonPath.read(JSON, "$.participants[0].stats.win");
        int matchtime = JsonPath.read(JSON, "$.gameDuration");

        Output.add(String.valueOf(champid));
        Output.add(String.valueOf(kills));
        Output.add(String.valueOf(deaths));
        Output.add(String.valueOf(assists));
        Output.add(String.valueOf(goldEarned));
        Output.add(String.valueOf(win));
        Output.add(String.valueOf(matchtime));

        return Output;
    }

    private String GetWhoPlayedChampion(BufferedReader reader, int champid) {
        String name = "";
        try {

            String champ = new ChampionIdToChampString().getChampName(champid);
            System.out.println("Who was playing: " + champ);

            name = reader.readLine();
            name = name.toLowerCase();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return name;
    }
}
