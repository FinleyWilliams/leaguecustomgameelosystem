import com.google.common.io.Resources;
import com.jayway.jsonpath.JsonPath;
import org.apache.commons.codec.Charsets;

import java.io.File;
import java.net.URL;

class EloCalc {

    void calculateElo(String name) {

        String PlayerData = null;

        File tmpDir = new File("C:/Users/Finley/IdeaProjects/LeagueMatchMaker/src/main/resources/Players/" + name + "Profile.json");
        boolean exists = tmpDir.exists();

        if (exists) {
            try {
                URL url = Resources.getResource("Players/" + name + "Profile.json");
                PlayerData = Resources.toString(url, Charsets.UTF_8);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        double scoreWithoutWin = 0;
        double score = 0;
        double totalScore = 0;
        int Length = JsonPath.read(PlayerData, "$.Matches.length()");
        for (int i = 0; i < Length; i++) {

            int kills = JsonPath.read(PlayerData, "$.Matches[" + i + "].Kills");
            int deaths = JsonPath.read(PlayerData, "$.Matches[" + i + "].Deaths");
            int assists = JsonPath.read(PlayerData, "$.Matches[" + i + "].Assists");
            int goldEarned = JsonPath.read(PlayerData, "$.Matches[" + i + "].Gold");
            Boolean win = JsonPath.read(PlayerData, "$.Matches[" + i + "].Win");
            int matchTime = JsonPath.read(PlayerData, "$.Matches[ " + i + "].matchTime");

            // (Gold/350) * (( K + (A * 0.5) ) / D) * (W / 1.25 : L / 0.75)

            double winMod;
            if (win) {
                winMod = 1.25;
            }
            else {
                winMod = 0.75;
            }
            scoreWithoutWin = (goldEarned / (matchTime / 60)) * ((kills + (assists * 0.5))/deaths) * winMod;
            score = (goldEarned / (matchTime / 60)) * ((kills + (assists * 0.5))/deaths) * winMod;
            totalScore = score + totalScore;
            System.out.println(scoreWithoutWin);
        }
        System.out.println(totalScore);
    }

}
